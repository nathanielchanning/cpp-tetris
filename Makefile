.PHONY: clean, run_test
flags =  -g
sdl2-flags = -lSDL2 -lSDL2_ttf
obj-files = board.o point.o tetromino.o tetris-board.o tetris-graphics.o

run_test: main_test
	./main_test

tetris: src/tetris.cc $(obj-files)
	g++ $(flags) $(sdl2-flags) $(obj-files) src/tetris.cc -o tetris

gdb: main_test
	gdb ./main_test

main_test: test/test.cc tests_main.o $(obj-files)
	g++ $(flags) $(sdl2-flags) $(obj-files) tests_main.o test/test.cc -o main_test -I src/

tetris-graphics.o: src/tetris-graphics.h src/tetris-graphics.cc tetris-board.o point.o
	g++ $(flags) -c src/tetris-graphics.cc

tetris-board.o: src/tetris-board.h src/tetris-board.cc
	g++ $(flags) -c src/tetris-board.cc	

board.o: src/board.h src/board.cc
	g++ $(flags) -c src/board.cc	

point.o: src/point.h src/point.cc
	g++ $(flags) -c src/point.cc

tetromino.o: src/tetromino.h src/tetromino.cc
	g++ $(flags) -c src/tetromino.cc

tests_main.o: test/tests_main.cc
	g++ $(flags) test/tests_main.cc -c

clean:
	rm *.o main_test tetris
