#include <SDL2/SDL.h>
#include "tetris-graphics.h"
#include "tetris-board.h"

int main () {
    Tetris_Graphics graphics;
    Tetris_Board board;
    SDL_Event event;
    unsigned int last_tick = SDL_GetTicks ();
    unsigned int shift_down_delta = 100;

    /* loop indefinitely. */
    while (1) {

        board. clear_full_rows ();

        board. reset_origin ();

        /* Check if a new tetromino can be
         * placed. */
        if (board. new_tetromino () == -1) {
            break;
        }

        board. set_tetromino_at_origin ();

        /* Continue moving it down while possible. */
        while (1) {

            graphics. render_board (board);

            /* Delay a little as to not 
             * bog down the CPU. */
            SDL_Delay (60);

            /* Check for events. */
            if (SDL_PollEvent (& event) != 0) {

                if (event. type == SDL_KEYDOWN) {
                    /* Change the tetromino state based on
                     * key events. */
                    switch (event. key. keysym. sym) {
                    case SDLK_UP:
                        board. rotate_tetromino_right ();
                        /* Prevent placement from
                         * immediately occurring after
                         * movement. */
                        last_tick = SDL_GetTicks ();
                        continue;

                    case SDLK_DOWN:
                        board. rotate_tetromino_left ();
                        last_tick = SDL_GetTicks ();
                        continue;

                    case SDLK_LEFT:
                        board. move_tetromino_left ();
                        last_tick = SDL_GetTicks ();
                        continue;

                    case SDLK_RIGHT:
                        board. move_tetromino_right ();
                        last_tick = SDL_GetTicks ();
                        continue;

                    /* Pause the game. */
                    case SDLK_p:
                        while (SDL_PollEvent (& event) == 0 ||
                               event. type != SDL_KEYDOWN)
                            SDL_Delay (60);
                        continue;

                    case SDLK_q:
                        goto end;
                    }
                }

            }

            /* Check to see if the tetromino should
             * be shifted down. */
            int current_tick = SDL_GetTicks ();

            /* This should only be relevant after ~49 days
             * of runtime. I imagine branch prediction
             * will make it irrelevant most of the time. */
            int difference = (current_tick < last_tick)? 
                                shift_down_delta + 1 : 
                                current_tick - last_tick;

            if (difference > shift_down_delta) {
                /* Update the last_tick. */
                last_tick = current_tick;

                if (board. move_tetromino_down ()) {
                    /* The tetromino can't be moved down. */
                    break;
                }
            }
        }
    }

end:
    return 0;
}
