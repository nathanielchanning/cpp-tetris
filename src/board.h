#ifndef BOARD_H
#define BOARD_H
#include "point.h"

class Board {
    /* These may not need to be public. */
  public:
    static const unsigned int height = 22;
    static const unsigned int width  = 10;
    static const int unoccupied = -1;
    Board ();
    int set_position (Point);
    int get_position (Point);
    int reset_position (Point);
    /* Shifts one row down. */
    int shift_row_down (int);
    int clear_row (int);
    /* Shifts all rows down. */
    int shift_down (int);
    int get_row_occupancy (int);
    void print ();
    
  private:
    /* This needs to be initialized to -1's
     * in the consrtuctor. */
    int board [height * width];
    /* number of positions used in the row. 
     *
     * This needs initialized to all 0's. */
    int row_occupancy [height];
};

#endif
