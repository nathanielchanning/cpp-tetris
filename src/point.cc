#include "point.h"

Point::Point (int x, int y, int color) {
    this-> x      =  x;
    this-> y      =  y;
    this-> color  =  color;
}

Point Point::operator+ (const Point & other) const {
/* The & in the parameter list says to pass the
 * actual object to the function, not a copy.
 * The const says that this object cannot
 * be modified. */
    int x = this-> x + other. x;
    int y = this-> y + other. y;
    int color = (this-> color > other. color)?  this-> color : other. color;

    return Point (x, y, color);
}
