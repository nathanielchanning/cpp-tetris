#include "tetromino.h"
#include <cstdlib>
#include <ctime>

/* This holds 7 tetrominos.
 *
 * Each of the 7 tetrominos holds 4 orientations,
 * representing the four possible ways the tetromino
 * can be flipped.
 *
 * In each of the 4 orientations, there are 4 points,
 * representing the four offsets from an origin 
 * that make up the tetromino. */
const Point Tetromino::tetrominos [7] [4] [4] = {
    /* Tetromino i */
    {
        { Point (-1, 0, 0x00FFFF), Point (0, 0, 0x00FFFF), Point (1, 0, 0x00FFFF), Point (2, 0, 0x00FFFF) },
        { Point (0, -1, 0x00FFFF), Point (0, 0, 0x00FFFF), Point (0, 1, 0x00FFFF), Point (0, 2, 0x00FFFF) },
        { Point (-1, 0, 0x00FFFF), Point (0, 0, 0x00FFFF), Point (1, 0, 0x00FFFF), Point (2, 0, 0x00FFFF) },
        { Point (0, -1, 0x00FFFF), Point (0, 0, 0x00FFFF), Point (0, 1, 0x00FFFF), Point (0, 2, 0x00FFFF) },
    },

    /* Tetromino j */
    {
        { Point (-1, 0, 0x0000FF), Point (0, 0, 0x0000FF), Point (1, 0, 0x0000FF), Point (-1, -1, 0x0000FF) },
        { Point (0, -1, 0x0000FF), Point (0, 0, 0x0000FF), Point (0, 1, 0x0000FF), Point (1, -1, 0x0000FF) },
        { Point (-1, 0, 0x0000FF), Point (0, 0, 0x0000FF), Point (1, 0, 0x0000FF), Point (1, 1, 0x0000FF) },
        { Point (0, -1, 0x0000FF), Point (0, 0, 0x0000FF), Point (0, 1, 0x0000FF), Point (-1, 1, 0x0000FF) },
    },

    /* Tetromino L */
    {
        { Point (-1, 0, 0xFFA500), Point (0, 0, 0xFFA500), Point (1, 0, 0xFFA500), Point (1, -1, 0xFFA500) },
        { Point (0, -1, 0xFFA500), Point (0, 0, 0xFFA500), Point (0, 1, 0xFFA500), Point (1, 1, 0xFFA500) },
        { Point (-1, 0, 0xFFA500), Point (0, 0, 0xFFA500), Point (1, 0, 0xFFA500), Point (-1, 1, 0xFFA500) },
        { Point (0, -1, 0xFFA500), Point (0, 0, 0xFFA500), Point (0, 1, 0xFFA500), Point (-1, -1, 0xFFA500) },
    },

    /* Tetromino o */
    {
        { Point (1, 0, 0xFFFF00), Point (0, 0, 0xFFFF00), Point (0, 1, 0xFFFF00), Point (1, 1, 0xFFFF00) },
        { Point (-1, 0, 0xFFFF00), Point (0, 0, 0xFFFF00), Point (0, 1, 0xFFFF00), Point (-1, 1, 0xFFFF00) },
        { Point (-1, 0, 0xFFFF00), Point (0, 0, 0xFFFF00), Point (-1, -1, 0xFFFF00), Point (0, -1, 0xFFFF00) },
        { Point (1, 0, 0xFFFF00), Point (0, 0, 0xFFFF00), Point (0, -1, 0xFFFF00), Point (1, -1, 0xFFFF00) },
    },

    /* Tetromino s */
    {
        { Point (1, 0, 0x00FF00), Point (0, 0, 0x00FF00), Point (0, 1, 0x00FF00), Point (-1, 1, 0x00FF00) },
        { Point (-1, 0, 0x00FF00), Point (0, 0, 0x00FF00), Point (-1, -1, 0x00FF00), Point (0, 1, 0x00FF00) },
        { Point (1, 0, 0x00FF00), Point (0, 0, 0x00FF00), Point (0, 1, 0x00FF00), Point (-1, 1, 0x00FF00) },
        { Point (-1, 0, 0x00FF00), Point (0, 0, 0x00FF00), Point (-1, -1, 0x00FF00), Point (0, 1, 0x00FF00) },
    },

    /* Tetromino t */
    {
        { Point (-1, 0, 0x800080), Point (0, 0, 0x800080), Point (0, -1, 0x800080), Point (1, 0, 0x800080) },
        { Point (0, 1, 0x800080), Point (0, 0, 0x800080), Point (0, -1, 0x800080), Point (1, 0, 0x800080) },
        { Point (-1, 0, 0x800080), Point (0, 0, 0x800080), Point (1, 0, 0x800080), Point (0, 1, 0x800080) },
        { Point (-1, 0, 0x800080), Point (0, 0, 0x800080), Point (0, -1, 0x800080), Point (0, 1, 0x800080) },
    },

    /* Tetromino z */
    {
        { Point (-1, 0, 0xFF0000), Point (0, 0, 0xFF0000), Point (1, 1, 0xFF0000), Point (0, 1, 0xFF0000) },
        { Point (-1, 0, 0xFF0000), Point (0, 0, 0xFF0000), Point (-1, 1, 0xFF0000), Point (0, -1, 0xFF0000) },
        { Point (-1, 0, 0xFF0000), Point (0, 0, 0xFF0000), Point (1, 1, 0xFF0000), Point (0, 1, 0xFF0000) },
        { Point (-1, 0, 0xFF0000), Point (0, 0, 0xFF0000), Point (-1, 1, 0xFF0000), Point (0, -1, 0xFF0000) },
    }
};

Tetromino::Tetromino () {
    srand (time (0));
    this-> tetromino = rand () % Tetromino::number_of_tetrominos;
    this-> orientation = 0;
}

Tetromino::Tetromino (int tetromino) {
    this-> tetromino = tetromino;
    this-> orientation = 0;
}

int Tetromino::rotate_left () {
    /* Use the remainder operator to bound it. */
    int result = (this-> orientation - 1) % 4;
    return this-> orientation = (result < 0)? 3 : result;
}

int Tetromino::rotate_right () {
    return this-> orientation = (this-> orientation + 1) % 4;
}

Point Tetromino::operator[] (int i) {
    /* Just going to mod by 4 because
     * I don't feel like catching any mistakes. */
    int index = (i < 0)? 3 : i % 4;
    return this-> tetrominos [this-> tetromino] [this-> orientation] [index];
}
