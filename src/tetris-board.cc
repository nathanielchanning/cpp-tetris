#include "tetris-board.h"
#include <iostream>

Tetris_Board::Tetris_Board () {
    this-> origin = Point (4, 2, Board::unoccupied);
    this-> rows_cleared = 0;

     /* -2 because we call
      * get_tetromino twice in a row at the
      * beginning to fill the next tetromino
      * and the current tetromino.*/
    this-> tetrominos_used = -2;

    /* Call it twice to populate the
     * current and next tetrominos. */
    this-> new_tetromino ();
    this-> new_tetromino ();
}

int Tetris_Board::reset_origin () {
    this-> origin = Point (4, 2, Board::unoccupied);

    return 0;
}

int Tetris_Board::set_tetromino_at_origin () {
    /* Check to see if the tetromino can be placed without
     * any collisions. */
    for (int i = 0; i < 4; i++) {
        Point temp_point = this-> current_tetromino [i];
        Point point = temp_point + this-> origin;

        /* Check if the position is inside
         * the board. */
        if (point. x >= Board::width  ||
            point. y >= Board::height ||
            point. x < 0              ||
            point. y < 0              ||
            /* Check if the position is already
             * occupied. */
            this-> get_position (point) != Board::unoccupied) {

            return -1;
        }
    }

    for (int i = 0; i < 4; i++) {
        Point temp_point = this-> current_tetromino [i];
        Point point = temp_point + this-> origin;

        this-> set_position (point);
    }

    return 0;
}

int Tetris_Board::reset_tetromino_at_origin () {
    for (int i = 0; i < 4; i++) {
        Point point = this-> current_tetromino [i] + this-> origin;

        /* Check if the position is inside
         * the board. */
        if (point. x >= Board::width  ||
            point. y >= Board::height ||
            point. x < 0              ||
            point. y < 0) {

            return -1;
        }

        this-> reset_position (point);
    }

    return 0;
}

int Tetris_Board::new_tetromino (int tetromino) {
    this-> current_tetromino = Tetromino (tetromino);

    if (this-> set_tetromino_at_origin ()) {
        return -1;
    }

    this-> tetrominos_used++;

    this-> reset_tetromino_at_origin ();

    return 0;
}

int Tetris_Board::new_tetromino () {
    this-> current_tetromino = this-> next_tetromino;

    this-> next_tetromino = Tetromino ();

    if (this-> set_tetromino_at_origin ()) {
        return -1;
    }

    this-> tetrominos_used++;

    this-> reset_tetromino_at_origin ();

    return 0;
}

Tetromino Tetris_Board::get_next_tetromino () {
    this-> tetrominos_used++;

    return this-> next_tetromino;
}

Point Tetris_Board::get_origin () {
    return this-> origin;
}

int Tetris_Board::get_rows_cleared () {
    return this-> rows_cleared;
}

int Tetris_Board::get_tetrominos_used () {
    return this-> tetrominos_used;
}

int Tetris_Board::move_origin (Point point_to_add) {
    Point point = this-> origin + point_to_add;

    /* Return -1 if invalid. */
    if (point. x >= Board::width  ||
        point. y >= Board::height ||
        point. x < 0              ||
        point. y < 0) {

        return -1;
    }

    this-> origin = point;

    return 0;
}

int Tetris_Board::move_origin_down () {
    Point point_to_add = Point (0, 1, -1);

    return move_origin (point_to_add);
}

int Tetris_Board::move_origin_up () {
    Point point_to_add = Point (0, -1, -1);

    return move_origin (point_to_add);
}

int Tetris_Board::move_origin_left () {
    Point point_to_add = Point (-1, 0, -1);

    return move_origin (point_to_add);
}

int Tetris_Board::move_origin_right () {
    Point point_to_add = Point (1, 0, -1);

    return move_origin (point_to_add);
}

int Tetris_Board::move_tetromino_down () {
    /* Remove current tetromino state from the board. */
    this-> reset_tetromino_at_origin ();

    if (this-> move_origin_down ()) {
        /* Reset tetromino state. */
        this-> set_tetromino_at_origin ();

        /* Indicate that it can't be lowered. */
        return -1;
    }

    /* Can it be placed at the new origin. */
    if (this-> set_tetromino_at_origin ()) {
        this-> move_origin_up ();
        this-> set_tetromino_at_origin ();

        return -1;
    }

    return 0;
}

int Tetris_Board::rotate_tetromino_left () {
    /* Remove current tetromino state from the board. */
    this-> reset_tetromino_at_origin ();

    this-> current_tetromino. rotate_left ();

    /* Can the new tetromino be placed? */
    if (this-> set_tetromino_at_origin ()) {
        this-> current_tetromino. rotate_right ();
        this-> set_tetromino_at_origin ();

        return -1;
    }

    return 0;

}

int Tetris_Board::rotate_tetromino_right () {
    /* Remove current tetromino state from the board. */
    this-> reset_tetromino_at_origin ();

    this-> current_tetromino. rotate_right ();

    /* Can the new tetromino be placed? */
    if (this-> set_tetromino_at_origin ()) {
        this-> current_tetromino. rotate_left ();
        this-> set_tetromino_at_origin ();

        return -1;
    }

    return 0;

}

int Tetris_Board::move_tetromino_left () {
    /* Remove current tetromino state from the board. */
    this-> reset_tetromino_at_origin ();

    if (this-> move_origin_left ()) {
        /* Reset tetromino state. */
        this-> set_tetromino_at_origin ();

        /* Indicate that it can't be lowered. */
        return -1;
    }

    /* Can it be placed at the new origin. */
    if (this-> set_tetromino_at_origin ()) {
        this-> move_origin_right ();
        this-> set_tetromino_at_origin ();

        return -1;
    }

    return 0;
}

int Tetris_Board::move_tetromino_right () {
    /* Remove current tetromino state from the board. */
    this-> reset_tetromino_at_origin ();

    if (this-> move_origin_right ()) {
        /* Reset tetromino state. */
        this-> set_tetromino_at_origin ();

        /* Indicate that it can't be lowered. */
        return -1;
    }

    /* Can it be placed at the new origin. */
    if (this-> set_tetromino_at_origin ()) {
        this-> move_origin_left ();
        this-> set_tetromino_at_origin ();

        return -1;
    }

    return 0;
}

int Tetris_Board::clear_full_rows () {
    /* Just going to run from top to bottom;
     * there's no need to be super efficient. */
    int full_rows = 0;

    for (int i = 0; i < Board::height; i++) {
        if (this-> get_row_occupancy (i) == Board::width) {
            full_rows++;

            /* Shift all the rows above it down. */
            this-> shift_down (i);
        }
    }

    this-> rows_cleared += full_rows;

    return full_rows;
}
