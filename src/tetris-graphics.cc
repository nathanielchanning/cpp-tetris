#include <iostream>
#include <stdio.h>
#include "tetris-graphics.h"
#include "tetromino.h"

Tetris_Graphics::Tetris_Graphics () {
    /* Add support for exceptions at some point. */

    /* Initialize SDL2. */
    if (SDL_Init (SDL_INIT_VIDEO)) {
        std::cerr << "Error: failure initializing SDL2---"
                  << SDL_GetError ()
                  << std::endl;
    }

    /* Create the window and renderer. */
    int return_value = SDL_CreateWindowAndRenderer (this-> width, this-> height, SDL_WINDOW_SHOWN, & this-> window, & this-> renderer);

    if (return_value) {
        std::cerr << "Error: failure creating window and renderer---"
                  << SDL_GetError ()
                  << std::endl;
    }

    /* Initialize the font subsystem. */
    if (TTF_Init ()) {
        std::cerr << "Error: failure initializing ttf---"
                  << SDL_GetError ()
                  << std::endl;
    }

    /* Grab the font for rendering
     * the stats. */
    this-> free_sans = TTF_OpenFont ("src/ttf/FreeSans.ttf", 24);

    if (this-> free_sans == NULL) {
        std::cerr << "Error: failure opening font---"
                  << SDL_GetError ()
                  << std::endl;
    }
}

Tetris_Graphics::~Tetris_Graphics () {
    TTF_Quit ();
    SDL_DestroyRenderer (this-> renderer);
    SDL_DestroyWindow (this-> window);

    SDL_Quit ();
}

int Tetris_Graphics::draw_point (Point point) {
    int r, g, b, a;

    /* Each part of the color is allocated
     * 8 bits. */
    b = (point. color)       & 0xFF;
    g = (point. color >> 8)  & 0xFF;
    r = (point. color >> 16) & 0xFF;

    SDL_SetRenderDrawColor (this-> renderer, r, g, b, 0xFF);

    int point_size = Tetris_Graphics::point_size;
    
    /* Multiply by point_size to avoid overlap. */
    SDL_Rect rectangle = { point. x * point_size,
                           point. y * point_size,
                           point_size,
                           point_size };

    SDL_RenderFillRect (this-> renderer, & rectangle);

    return 0;
}

int Tetris_Graphics::clear_screen () {
    /*                                 R    G    B    A */
    SDL_SetRenderDrawColor (renderer, 0x0, 0x0, 0x0, 0xFF);
    SDL_RenderClear (this-> renderer);

    return 0;
}

int Tetris_Graphics::render_next_tetromino (Tetris_Board board) {
    Point offset = Point (board. width + 3, 3);

    for (int i = 0; i < 4; i++) {
        Tetromino next_tetromino = board. get_next_tetromino ();
        Point current_point = next_tetromino [i];

        draw_point (current_point + offset);
    }

    return 0;
}

int Tetris_Graphics::render_stats (Tetris_Board board) {
    char string_buffer [100];

    snprintf (string_buffer, 100, "Rows: %d Tetrominos: %d", board. get_rows_cleared (), board. get_tetrominos_used ());

    SDL_Color white = {255, 255, 255};
    SDL_Rect position = {
        .x = board. width * this-> point_size,
        .y = 5 * this-> point_size,
        .w = 200,
        .h = 32
    };

    SDL_Surface * stats_surface = TTF_RenderText_Solid (this-> free_sans, string_buffer, white);
    SDL_Texture * stats_texture = SDL_CreateTextureFromSurface (this-> renderer, stats_surface);

    SDL_RenderCopy (this-> renderer, stats_texture, NULL, & position);

    /* free the texture and surface. */
    SDL_DestroyTexture (stats_texture);
    SDL_FreeSurface (stats_surface);

    return 0;
}

int Tetris_Graphics::render_board (Tetris_Board board) {
    Point current_point;

    this-> clear_screen ();

    /* Loop across each row. */
    for (current_point. y = 2; current_point. y < board. height; current_point. y++) {
        for (current_point. x = 0; current_point. x < board. width; current_point. x++) {
            current_point. color = board. get_position (current_point);
            /* Change the background color. */
            if (current_point. color == -1) {
                current_point. color = 0x999999;
            }

            this-> draw_point (current_point);
        }
    }

    this-> render_next_tetromino (board);

    this-> render_stats (board);

    /* Render the current state. */
    SDL_RenderPresent (this-> renderer);

    return 0;
}
