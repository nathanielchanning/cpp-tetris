#ifndef TETRIS_BOARD_H
#define TETRIS_BOARD_H
#include "board.h"
#include "tetromino.h"
#include "point.h"

class Tetris_Board: public Board {
  public:
    Tetris_Board ();
    int reset_origin ();
    int set_tetromino_at_origin ();
    int reset_tetromino_at_origin ();
    int new_tetromino (int);
    int new_tetromino ();
    Tetromino get_next_tetromino ();
    Point get_origin ();
    int get_rows_cleared ();
    int get_tetrominos_used ();
    int rotate_tetromino_left ();
    int rotate_tetromino_right ();
    int move_tetromino_down ();
    int move_tetromino_left ();
    int move_tetromino_right ();
    int clear_full_rows ();

  private:
    int rows_cleared, tetrominos_used;
    Point origin;
    Tetromino current_tetromino;
    Tetromino next_tetromino;
    int move_origin (Point point_to_add);
    int move_origin_left ();
    int move_origin_right ();
    int move_origin_up ();
    int move_origin_down ();
};

#endif
