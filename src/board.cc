#include "board.h"
#include <iostream>
#include <algorithm>

/* This is needed for std::fill_n so that
 * a variable will exist to be passed to it
 * by reference. */
const int Board::unoccupied;

Board::Board () {
    std::fill_n (this-> board, Board::height * Board::width, Board::unoccupied); 
    std::fill_n (this-> row_occupancy, Board::height, 0);
}

int Board::get_position (Point point) {
    /* Returns -2 if the position can't be reached. */
    if (point. x >= Board::width  ||
        point. y >= Board::height ||
        point. x < 0             ||
        point. y < 0) {

        std::cerr << "Invalid point passed to set_position"
                  << std::endl
                  << " x: " << point. x
                  << " y: " << point. y
                  << std::endl;

        return -2;
    }

    return this-> board [point. y * Board::width + point. x];
}

int Board::set_position (Point point) {
    if (point. x >= Board::width  ||
        point. y >= Board::height ||
        point. x < 0             ||
        point. y < 0) {

        std::cerr << "Invalid point passed to set_position"
                  << std::endl
                  << " x: " << point. x
                  << " y: " << point. y
                  << std::endl;

        return -1;
    }

    if (this-> board [point. y * Board::width + point. x] != Board::unoccupied) {
        /* The space is already occupied;
         * return 0 as an indication. */
        return 0;
    } 

    this-> board [point. y * Board::width + point. x] = point. color;
    this-> row_occupancy [point. y]++;

    return 1;
}

int Board::reset_position (Point point) {
    if (point. x >= Board::width  ||
        point. y >= Board::height ||
        point. x < 0             ||
        point. y < 0) {

        std::cerr << "Invalid point passed to set_position"
                  << std::endl
                  << " x: " << point. x
                  << " y: " << point. y
                  << std::endl;

        return -1;
    }
    
    this-> board [point. y * Board::width + point. x] = Board::unoccupied;
    this-> row_occupancy [point. y]--;

    return 0;
}

int Board::shift_row_down (int row_num) {
    if (row_num >= Board::height ||
            row_num < 0) {
        std::cerr << "Invalid row number passed to "
                  << "shift_row_down."
                  << std::endl
                  << row_num
                  << std::endl;

        return -1;
    }

    int upper_row = Board::width * row_num;
    int lower_row = Board::width * (row_num + 1);

    for (int i = 0; i < Board::width; i++) {
        this-> board [lower_row + i] = this-> board [upper_row + i];
    }

    this-> row_occupancy [row_num + 1] = this-> row_occupancy [row_num];
    this-> row_occupancy [row_num] = 0;

    this-> clear_row (row_num);
    
    return 0;
}

int Board::clear_row (int row_num) {
    if (row_num >= Board::height ||
            row_num < 0) {
        std::cerr << "Invalid row number passed to "
                  << "shift_row_down."
                  << std::endl
                  << row_num
                  << std::endl;

        return -1;
    }

    int row = row_num * Board::width;

    for (int i = 0; i < Board::width; i++) {
        this-> board [row + i] = Board::unoccupied;
    }

    return 0;
}

int Board::shift_down (int row_num) {
/* Shifts down the rows above row_num. */
    if (row_num >= Board::height ||
            row_num < 0) {
        std::cerr << "Invalid row number passed to "
                  << "shift_down."
                  << std::endl
                  << row_num
                  << std::endl;

        return -1;
    }

    for (int i = row_num - 1; i >= 0; i--) {
        this-> shift_row_down (i);
    }

    this-> clear_row (0);

    return 0;
}

int Board::get_row_occupancy (int row_num) {
    if (row_num < 0 ||
        row_num >= Board::height) {

        return -1;
    }

    return this-> row_occupancy [row_num];
}

void Board::print () {
    for (int i = 0; i < Board::height * Board::width; i++) {
        if (i % 10 == 0) {
            std::cout << this-> row_occupancy [i / Board::width]
                      << " | ";
        }

        std::cout << this-> board [i] << " ";

        if (i % 10 == 9) {
            std::cout << std::endl;
        }
    }
}
