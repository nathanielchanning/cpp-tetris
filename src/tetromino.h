#ifndef TETROMINO_H
#define TETROMINO_H
#include "point.h"

class Tetromino
{
  private:
      int tetromino;
      int orientation;

  public:
      static const int number_of_tetrominos = 7;
      /* The actual literal definition can't
       * be assigned in the class definition,
       * so it is placed in the implementation file. */
      static const Point tetrominos [number_of_tetrominos] [4] [4];
      Tetromino ();
      Tetromino (int tetromino);
      int rotate_left ();
      int rotate_right ();
      Point operator[] (int);
};

#endif
