#ifndef POINT_H
#define POINT_H

class Point {
  public:
    int x, y;
    int color;
    Point (int x = 0, int y = 0, int color = 0);
    /* Constructor delegation--requires C++11. 
     * We can also use a default value with
     * the above constructor:
     *
     * Point (int x, int y, int color = 0);*/
//    Point (int x, int y) : Point (x, y, 0) {};
    Point operator+ (const Point & other) const;
};

#endif
