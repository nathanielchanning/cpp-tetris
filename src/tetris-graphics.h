#ifndef TETRIS_GRAPHICS_H
#define TETRIS_GRAPHICS_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "tetris-board.h"
#include "point.h"

class Tetris_Graphics {
  private:
    static const int point_size = 20;
    SDL_Window * window;
    SDL_Renderer * renderer;
    TTF_Font * free_sans;

    int draw_point (Point point);
    int clear_screen ();
    int render_next_tetromino (Tetris_Board board);
    int render_stats (Tetris_Board board);

  public:
    static const int width = 640;
    static const int height = 480;

    Tetris_Graphics ();
    ~Tetris_Graphics ();

    int render_board (Tetris_Board board);
};

#endif
