#include "catch.hpp"
#include "board.h"
#include "point.h"
#include "tetromino.h"
#include "tetris-board.h"
#include "tetris-graphics.h"
#include <iostream>
#include <unistd.h>

TEST_CASE ("point can be created and added.", "[point]") {
    Point point_1 (0, 0);
    Point point_2 (10, 10, 1);

/*--------------------------Point------------------------------------*/
    SECTION ("correct values are set during construction.") {
        REQUIRE (point_1. x     == 0);
        REQUIRE (point_1. y     == 0);
        /* 0 is the default color 
         * for the 2 argument constructor. */
        REQUIRE (point_1. color == 0);

        REQUIRE (point_2. x     == 10);
        REQUIRE (point_2. y     == 10);
        REQUIRE (point_2. color == 1);
    }

/*--------------------------operator+--------------------------------*/
    SECTION ("points can be added together.") {
        Point result_point = point_1 + point_2;

        REQUIRE (result_point. x == 10); 
        REQUIRE (result_point. y == 10);
        REQUIRE (result_point. color == 1);
    }
}

TEST_CASE ("board can get, set, and reset positions.", "[board]") {
    Board board;
    Point invalid_point (Board::width + 1, Board::height + 1);
    Point point (0, 0, 1);

/*--------------------------get_position-----------------------------*/
    SECTION ("invalid points return -2 with get.") {
        REQUIRE (board. get_position (invalid_point) == -2);
    }

    SECTION ("valid points return their value with get.") {
        REQUIRE (board. get_position (point) == -1);
    }

/*--------------------------set_position-----------------------------*/
    SECTION ("valid points return 1 when set.") {
        REQUIRE (board. set_position (point) == 1);
    }

    SECTION ("invalid points return -1 when set.") {
        REQUIRE (board. set_position (invalid_point) == -1);
    }

    SECTION ("already occupied positions return 0 when set.") {
        board. set_position (point);

        REQUIRE (board. set_position (point) == 0);
    }

    SECTION ("correctly sets the position to the color.") {
        board. set_position (point);

        REQUIRE (board. get_position (point) == point. color);
    }

/*------------------------reset_position-----------------------------*/
    /* Add tests here. */
}

TEST_CASE ("tetromino can be created and rotated.", "[tetromino]") {

/*------------------------Tetromino-&-rotate-------------------------*/
    SECTION ("does not crash when created and rotated.") {
        Tetromino tetromino (0);

        tetromino. rotate_left ();
        tetromino. rotate_right ();
    }

/*------------------------Tetromino-&-rotate-------------------------*/
    SECTION ("does not crash on random creation and rotation.") {
        Tetromino tetromino; 

        tetromino. rotate_right ();
        tetromino. rotate_left ();
    }
}

TEST_CASE ("board can clear rows and shift rows down.", "[board]") {
    Board board;

    Point pre_points [Board::width] = {
        Point (0, 0, 1), Point (1, 0, 1),
        Point (2, 0, 1), Point (3, 0, 1),
        Point (4, 0, 1), Point (5, 0, 1),
        Point (6, 0, 1), Point (7, 0, 1),
        Point (8, 0, 1), Point (9, 0, 1),
    };

    Point post_points [Board::width] = {
        Point (0, 1, 1), Point (1, 1, 1),
        Point (2, 1, 1), Point (3, 1, 1),
        Point (4, 1, 1), Point (5, 1, 1),
        Point (6, 1, 1), Point (7, 1, 1),
        Point (8, 1, 1), Point (9, 1, 1),
    };

    for (int i = 0; i < Board::width; i++) {
        board. set_position (pre_points [i]);
    }

/*--------------------------clear_row--------------------------------*/
    SECTION ("rows can be cleared.") {
        board. clear_row (0);

        for (int i = 0; i < Board::width; i++) {
            REQUIRE (-1 == board. get_position (pre_points [i]));
        }
    }

/*--------------------------shift_row_down---------------------------*/
    SECTION ("rows can be shifted down.") {
        board. shift_row_down (0);

        for (int i = 0; i < Board::width; i++) {
            REQUIRE (1 == board. get_position (post_points [i]));
        }
    }

/*--------------------------shift_down-------------------------------*/
}

TEST_CASE ("tetris board can set and remove tetrominos at the origin.", "[tetris-board]") {
    Tetris_Board board;

    SECTION ("the tetromino can be set at the origin.") {
        board. new_tetromino (0);
        board. set_tetromino_at_origin ();

        for (int i = 0; i < 4; i++) {
            Point point = Tetromino::tetrominos [0] [0] [i] + board. get_origin ();
            int color = Tetromino::tetrominos [0] [0] [i]. color;

            REQUIRE (board. get_position (point) == color);
        }
    }

    SECTION ("the tetromino can be reset at the origin.") {
        board. new_tetromino (0);
        board. set_tetromino_at_origin ();
        board. reset_tetromino_at_origin ();

        for (int i = 0; i < 4; i++) {
            Point point = Tetromino::tetrominos [0] [0] [i] + board. get_origin ();
            REQUIRE (board. get_position (point) == Board::unoccupied);
        }
    }
}

TEST_CASE ("tetris board can lower and rotate the current tetromino.", "[tetris-board]") {
    Tetris_Board board;
    board. new_tetromino (0);

    SECTION ("the current tetromino can be lowered.") {
        Point old_origin =  board. get_origin ();
        Point expected_origin = old_origin + Point (0 ,1);

        /* Fail if move_tetromino_down doesn't
         * return 0. */
        if (board. move_tetromino_down ()) {
            REQUIRE (0 == 1);
        }

        for (int i = 0; i < 4; i++) {
            Point point = Tetromino::tetrominos [0] [0] [i] + expected_origin;
            int color = Tetromino::tetrominos [0] [0] [i]. color;

            REQUIRE (board. get_position (point) == color);
        }
    }

    SECTION ("the current tetromino can be rotated left.") {
        /* Fail if rotate_left doesn't
         * return 0. */
        if (board. rotate_tetromino_left ()) {
            REQUIRE (0 == 1);
        } 

        for (int i = 0; i < 4; i++) {
            Point point = Tetromino::tetrominos [0] [3] [i] + board. get_origin ();
            int color = Tetromino::tetrominos [0] [3] [i]. color;

            REQUIRE (board. get_position (point) == color);
        }
    }

    SECTION ("the current tetromino can be rotated right.") {
        /* Fail if rotate_left doesn't
         * return 0. */
        if (board. rotate_tetromino_right ()) {
            REQUIRE (0 == 1);
        } 

        for (int i = 0; i < 4; i++) {
            Point point = Tetromino::tetrominos [0] [1] [i] + board. get_origin ();
            int color = Tetromino::tetrominos [0] [1] [i]. color;

            REQUIRE (board. get_position (point) == color);
        }
    }
}
